//this receives messages from the database saver 
// and fires them back out to any tabs that are running an IMDb title page
// we can't send messages directly between tabs from a chrome extension
// so we send one to the background page which then forwards the message back out

// Reminder: This fires twice b/c the net worther and the regular actor data both get saved separately

chrome.runtime.onMessage.addListener(
	message => {
		// console.log('background page - message', message);
		// if (message.test) {
		// 	return;
		// }

		chrome.tabs.query({
				url: 'https://www.imdb.com/title/*'
			},
			tabs => tabs.forEach(
				tab => chrome.tabs.sendMessage(tab.id, message))
		);
	});