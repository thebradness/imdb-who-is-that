// - from https://github.com/xpl/crx-hotreload/blob/master/hot-reload.js

REFRESH_INTERVAL = 1000;

chrome.management.getSelf(self => setup(self));

function setup(self) {
	if (self.installType != 'development') return; //dev only!

	chrome.runtime.getPackageDirectoryEntry(dir => watchChanges(dir));

	chrome.tabs.query({
			//"permissions": ["tabs"] in the manifest gives me access to the title property
			// this makes sure to only refresh tabs with IMDb on them
			title: '*IMDb',
			// active: true, //the selected tab for that window
		},
		tabs => tabs[0] && chrome.tabs.reload(tabs[0].id)
	);
}

function watchChanges(dir, lastTimestamp) {
	timestampForFilesInDirectory(dir)
		.then(timestamp => {
			const hasChanged = lastTimestamp && lastTimestamp != timestamp;
			if (hasChanged)
				chrome.runtime.reload();
			else
				setTimeout(() => watchChanges(dir, timestamp), REFRESH_INTERVAL);
		});
}

const timestampForFilesInDirectory = dir =>
	//build one giant string of all filenames and their current timestamps
	filesInDirectory(dir).then(files =>
		files.map(f => f.name + f.lastModifiedDate).join());

const filesInDirectory = dir => new Promise(resolve =>
	dir.createReader().readEntries(entries =>
		Promise.all(
			entries
			.filter(e => e.name[0] != '.')
			.map(e =>
				e.isDirectory ?
				filesInDirectory(e) :
				new Promise(resolve => e.file(resolve))
			))
		.then(files => [].concat(...files))
		.then(resolve)
	)
);