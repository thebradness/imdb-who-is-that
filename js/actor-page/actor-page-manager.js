class ActorPageManager {

	actorData;

	run() {
		this.buildDataObj();

		const self = this;
		$(document).ready(function() {
			self.getNetWorth();
			self.scrapeActorPage();
		});
	}

	buildDataObj() {
		const actorId = window.location.pathname.split('/')[2];
		// need to do name here b/c it's used by the net worth API
		const actorName = $('body').find(Selectors.ACTOR_NAME).text();

		this.actorData = {
			id: actorId, 
			actor_info: {
				id: actorId,
				name: actorName
			}
		};
	}

	getNetWorth() {
		const netWorther = new NetWorther(this.actorData);
		netWorther.fetch();
	}

	scrapeActorPage() {
		const hasLazyLoader = document.querySelector(Selectors.SEE_ALL_BUTTON);

		if (hasLazyLoader) {
			this.watchDom();
			this.clickSeeAllBtn();
		} else {
			this.startScraping();
		}
	}

	clickSeeAllBtn() {
		document
			.querySelector(Selectors.SEE_ALL_BUTTON)
			.click();
	}

	startScraping() {
		new ActorScraper(this.actorData).scrape();
	}

	watchDom() {
		//wait for the new records to come into the page
		// (from https://stackoverflow.com/a/11546242/2200951)
		const observer = new window
			.MutationObserver((mutations, observer) =>
				this.startScraping());

		//the see-all button will be replaced with a close button
		// (once the data is loaded in)
		// so we watch it's parent's children for changes
		// and then we know the actors are done loading in
		const seeAllButtonParentElement = document
			.querySelector('.filmo-section-actress, .filmo-section-actor')
			.nextElementSibling
			.querySelector('.has-content');

		observer.observe(seeAllButtonParentElement, {
			childList: true //allows this to fire when children are added
		});
	}
}