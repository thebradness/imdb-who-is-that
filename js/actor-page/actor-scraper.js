class ActorScraper {
	static DASH = /–|-/; // long dash or short dash
	static CURRENT_YEAR = new Date().getFullYear();
	static NEW_LINE = '&#010;';

	actorData;
	$actorPage;

	constructor(actorData) {
		this.actorData = actorData;
	}

	scrape() {
		this.$actorPage = $('body');
		this.buildActorData();
	}

	buildActorData() {
		this.scrapePhoto();
		this.scrapeBirthInfo(); //must be first
		this.scrapeDeathInfo();

		this.scrapeCredits();
		this.scrapeKnownFors();
		this.scrapeCreditCounts();
		this.buildChartValues();

		database.save(this.actorData);
	}

	getVis(isShown) {
		return ScrapeUtil.getVis(isShown);
	}

	scrapePhoto() {
		const headshot = this.$actorPage
			.find(Selectors.ACTOR_PHOTO);

		const headShotUrl = this.getLargestImg($(headshot));
		this.actorData.actor_info.photo = headShotUrl;
	}

	scrapeBirthInfo() {
		const birthInfo = this.$actorPage
			.find(Selectors.BIRTH_INFO)
			.text()
			.match(/([0-9]{4})(.*)/);

		const actorInfo = this.actorData.actor_info;
		actorInfo.has_birth_info = this.getVis(birthInfo);
		actorInfo.has_no_birth_info = this.getVis(!birthInfo);

		if (!birthInfo) {
			return;
		}

		const birthYear = birthInfo[1];

		actorInfo.age = ActorScraper.CURRENT_YEAR - birthYear;
		actorInfo.birth_year = birthYear;
		actorInfo.birth_place = birthInfo[2];
	}

	scrapeDeathInfo() {
		const deathInfo = this.$actorPage
			.find(Selectors.DEATH_INFO)
			.text()
			.match(/([0-9]{4})(.*)/);

		const actorInfo = this.actorData.actor_info;
		const hasBirthYear = actorInfo.birth_year;

		actorInfo.has_life = this.getVis(hasBirthYear && !deathInfo);
		actorInfo.has_after_life = this.getVis(hasBirthYear && deathInfo);

		if (!deathInfo || !actorInfo.birth_year) {
			return;
		}

		actorInfo.death_age = deathInfo[1] - actorInfo.birth_year;
		actorInfo.death_place = deathInfo[2]
			.split(/(?=\()/).join(ActorScraper.NEW_LINE);
		//split on paren but keep it
		//(cause of death is in parens)
	}

	scrapeCredits() {
		const self = this;
		const credits = [];
		const actorInfo = this.actorData.actor_info;
		const birthYear = actorInfo.birth_year;

		const actorScraper = this;

		this.$actorPage
			.find(Selectors.ACTING_CREDIT_WRAP)
			.each(function() {

				const $credit = $(this);
				const getText = selector => $credit.find(selector).text();

				const credit = {
					year: getText(Selectors.YEAR),
					title: getText(Selectors.TITLE),
					episode_count: parseInt(getText(Selectors.EPISODES)),
				};

				credit.has_episodes = self.getVis(credit.episode_count);

				//adding these this way is the easiest option 
				//since the credits are just another template that takes a single object
				credit.has_birth_info = actorScraper.getVis(actorInfo.birth_year);
				credit.has_no_birth_info = actorScraper.getVis(!actorInfo.birth_year);

				credit.id = $credit
					.find(Selectors.TITLE)
					.attr('href')
					.split('/')[2];

				credit.age = !birthYear ?
					'' :
					credit.year
					.split(ActorScraper.DASH)
					.map(year => year - birthYear)
					.join('-'); // short dash

				credit.character = $credit
					.find(Selectors.CHARACTER)
					.toArray()
					.map(ch => $(ch).text())
					.join(' / ');

				credits.push(credit);
			});

		this.actorData.credits = credits;
		this.actorData.actor_info.acting_credit_count = credits.length;
	}

	scrapeCreditCounts() {
		const creditTotal = this.$actorPage
			.find('.credits-total')
			.toArray() //convert to normal array
			.map(ct => +$(ct).text())
			.reduce((sum, sectionCount) => sum + sectionCount);

		this.actorData.actor_info.total_credit_count = creditTotal;
	}

	scrapeKnownFors() {
		const self = this;
		const knownFors = [];
		const actorCredits = this.actorData.credits;

		this.$actorPage
			.find(Selectors.KNOWN_FOR_WRAP)
			.each(function() {

				const titleId = $(this).find(Selectors.KNOWN_FOR_URL)
					.attr('href').split('/')[2];

				const matchingCredit = actorCredits.find(c => c.id == titleId);

				if (!matchingCredit) {
					//actors might have a known for where they were the director
					// it won't be found so just skip it
					return;
				}

				const knownFor = JSON.parse(JSON.stringify(matchingCredit));

				knownFor.src = self.getLargestImg($(this));

				const nl = ActorScraper.NEW_LINE;
				knownFor.title += nl +

					knownFor.character + nl +

					knownFor.year +

					(knownFor.age ? ` (age ${knownFor.age})` : '') +

					(knownFor.episode_count ? `${nl}${knownFor.episode_count} episode` : '');

				if (knownFor.episode_count > 1) {
					knownFor.title += 's';
				}

				knownFors.push(knownFor);
			});

		knownFors.sort((k1, k2) => {
			let start = k1.year.split(ActorScraper.DASH);
			let end = k2.year.split(ActorScraper.DASH);

			start = start[1] || start[0];
			end = end[1] || end[0];

			return +end - +start;
		});

		this.actorData.known_fors = knownFors;
	}

	getLargestImg(wrapperObj) {
		/* the src attribute has a smaller (lower-res) photo
		but all img tags have several image urls under "srcset" 
		ordered by size ascending so just grab the last one
		each url is followed by "90w," or "130w," so this splits on that */

		const imgUrls = wrapperObj.find('img').attr('srcset');
		if (!imgUrls) {
			return ''; //no images, avoid npe above
		}

		const imgUrlSplit = imgUrls.split(/\s[0-9]{2,3}w,?\s?/);
		const imgUrl = imgUrlSplit[imgUrlSplit.length - 2];

		return imgUrl;
	}

	buildChartValues() {
		const yearCountMap = {};

		//go through all the actor's credits and make a map of how many
		// credits they have for each year

		for (let credit of this.actorData.credits) {
			const span = credit.year.split(ActorScraper.DASH);
			const start = +span[0];
			const end = +span[1] || start;

			//these are filterable values so we can click on a chart year
			// - and filter the credits to everything for that year
			const yearSpan = [];

			for (let year = start; year <= end; year++) {
				yearSpan.push(year);
				yearCountMap[year] = ++yearCountMap[year] || 1;
			}

			credit.year_span = yearSpan.join(',');
		}

		this.actorData.chart_values = yearCountMap;
	}
}