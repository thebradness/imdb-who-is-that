class ScrapeUtil {
	static getVis(isShown) {
		const displayVal = isShown ? 'inline-flex' : 'none';
		return `style="display: ${displayVal}"`;
	}
}