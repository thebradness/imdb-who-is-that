class Selectors {

	//UI ELEMENTS
	static CLEAR_BUTTON = '#clear-filter-btn';
	static SEARCH_FIELD = '#credits-filter input';
	static REFRESH_BUTTON = '#refresh-btn';
	static REFRESH_ICON = '#refresh-icon';
	static REFRESH_LABEL = '#refresh-label';

	//PAGE ELEMENTS
	static SERIES_LINK = '[data-testid$=series-link], [itemprop=url]'; //2 different episode pages
	static SEE_ALL_BUTTON = '[data-testid^=nm-flmg-paginated-all-act]';

	//ACTOR INFO
	static BIRTH_INFO = '[data-testid="nm_pd_bl"]';
	static DEATH_INFO = '[data-testid="nm_pd_dl"]';
	static ACTOR_NAME = '[data-testid="hero__pageTitle"] span:first';
	static ACTOR_PHOTO = '.ipc-poster__poster-image:first';
	static KNOWN_FOR_WRAP = '[data-testid^=nm_kwn_for]';
	static KNOWN_FOR_URL = '.ipc-poster a';

	//ACTOR CREDITS
	static ACTING_CREDIT_WRAP = '[data-testid^=cred_act]';
	static PREFIX = '.ipc-metadata-list-summary-item_';
	static ITEM = `${this.PREFIX}_li`;

	static TITLE = `${this.PREFIX}_t`;
	static YEAR = `${this.PREFIX}_ctl ${this.ITEM}`;
	static EPISODES = `${this.PREFIX}_cbl ${this.ITEM}`;
	static CHARACTER = '.credit-text-list .ipc-inline-list__item';
}