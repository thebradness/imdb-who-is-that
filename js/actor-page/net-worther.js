class NetWorther {
	static NET_WORTH_UNITS = ['K', 'M', 'B', 'T'];
	static URL = 'https://api.api-ninjas.com/v1/celebrity?name=';

	actorData;

	constructor(actorData) {
		this.actorData = actorData;
	}

	fetch() {
		const self = this;
		const actorInfo = this.actorData.actor_info;
		actorInfo.has_net_worth = ScrapeUtil.getVis(false);
		actorInfo.has_no_net_worth = ScrapeUtil.getVis(false);

		if (!NET_WORTH_API_KEY) {
			return;
		}

		//above, we don't have an API key so we want to hide both
		// but if we get here, we want to assume there's no networth
		actorInfo.has_no_net_worth = ScrapeUtil.getVis(true);

		$.ajax({
			headers: {
				'X-Api-Key': NET_WORTH_API_KEY
			},
			url: `${NetWorther.URL}${this.actorData.actor_info.name}`,
			success: function(result) {
				if (!result.length) {
					database.save(self.actorData); //have to report there's no net worth
					return;
				}

				let places = -1;
				let worth = result[0].net_worth;

				while (worth >= 1000) {
					places++;
					worth /= 1000
				}

				const netWorthUnit = NetWorther.NET_WORTH_UNITS[places];

				actorInfo.net_worth = `$${worth} ${netWorthUnit}`;
				actorInfo.has_net_worth = ScrapeUtil.getVis(true);
				actorInfo.has_no_net_worth = ScrapeUtil.getVis(false);
				
				database.save(self.actorData);
			}
		});
	}
}