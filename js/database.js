class Database {

	static TABLE_NAME = 'actors';

	db;
	firstUpdateRequestMade = false;

	/*
		I hit the localStorage limit pretty quick so indexedDB is more industrial 
		and has a way higher limit

		Everything is asynchronous and non-blocking by default so you have to 
		create a request object everytime and stick onsuccess / onerror methods on it

		Good info here: https://javascript.info/indexeddb
	*/

	setup(onFinished) {
		const dbRequest = indexedDB.open('imdw', 1); //open or create database

		dbRequest.onupgradeneeded = event => { 
			//we end up here if there's no database (with that name) 
			// or the db version with that name has a lower version 
			// event.oldVersion will tell us the current version 
			// and 0 means no database at all
			
			//create a table
			// a full object can go straight into the table 
			// it just has to have an `id` prop at the root level (as we've stated here)
			// we can only create tables from inside this function
			// we could check if the table exists with:
			// `dbRequest.objectStoreNames.contains('books')`
			dbRequest.result.createObjectStore(Database.TABLE_NAME, {keyPath: 'id'});
		};

		dbRequest.onsuccess = () => {
			//if the db already exists, we come straight here
			//if setup above went smoothly, we come here next

			this.db = dbRequest.result;
			onFinished();
		};
	}

	get(actorId, onSuccess) {
		const transaction = this.db.transaction(Database.TABLE_NAME, 'readwrite');
		const table = transaction.objectStore(Database.TABLE_NAME);
		const request = table.get(actorId);

		request.onsuccess = () => onSuccess(request.result);
		request.onerror = () => console.log('IMDW - item NOT gotten:', request);
	}

	save(actorData) {
		if (!this.firstUpdateRequestMade && NET_WORTH_API_KEY) {
			// b/c saves are async, we can't request > update > save 
			// the same record at the same time without concurrency issues
			// so the actor scraper and the net worther update the same object and try to save
			// we don't do a real save until we've heard something from both
			this.firstUpdateRequestMade = true;
			return;
		}

		const transaction = this.db.transaction(Database.TABLE_NAME, 'readwrite');
		const table = transaction.objectStore(Database.TABLE_NAME);
		const request = table.put(actorData); //insert or update

		request.onsuccess = () => chrome.runtime.sendMessage(actorData);
		request.onerror = () => console.log('IMDW - item NOT saved:', request);
	}
}