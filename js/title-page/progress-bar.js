class ProgressBar {

	total;
	barElement;

	html = `<div id="imdw-progress-bar">` +
		`Saving actor(s): ` +
		`<span id="current">0</span>` +
		` of ` +
		`<span id="total">0</span>` +
		`</div>`;

	start(total) {
		this.total = total;
		this.barElement = $(this.html);

		$('body').append(this.barElement);
		this.barElement.find('#total').text(total);
	}

	update(current) {
		this.barElement.find('#current').text(current);

		if (current == this.total) {
			setTimeout(() => this.barElement.remove(), 1000);
		}
	}
}