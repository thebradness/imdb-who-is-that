class TitlePageManager {
	HAS_DATA_CSS_CLASS = 'actor-has-data';

	lastDisplayedActorId;

	actorCount = 0;
	processedActorCount = 0;
	unprocessedActorCount = 0;

	iframeActorIds = [];
	progressBar = new ProgressBar();
	displayPanel = new DisplayPanel();

	run() {
		this.displayPanel.setup();

		this.watchStorage();
		this.loopActorPhotos();
	}

	loopActorPhotos() {
		const self = this;
		const $actorPhotos = $('.ipc-avatar, .cast_list .primary_photo'); //top cast page, all cast page
		this.actorCount = $actorPhotos.length;

		$actorPhotos.each(function() {
			const $photo = $(this);
			const actorId = $photo.find('a').attr('href').split('/')[2];

			self.addPhotoData(actorId, $photo);
			self.addPhotoHover(actorId, $photo);
		});
	}

	addPhotoData(actorId, $photo) {

		database.get(actorId, actorData => {
			if (actorData) {
				this.processedActorCount++;
				$photo.addClass(this.HAS_DATA_CSS_CLASS);
			} else {
				this.unprocessedActorCount++;
				this.iframeActorIds.push(actorId);
			}

			const doneLoopingPhotos = this.processedActorCount + this.unprocessedActorCount == this.actorCount;
			
			if (this.iframeActorIds.length && doneLoopingPhotos) {
				this.progressBar.start(this.unprocessedActorCount);
				TitlePageUtil.makeActorIframe(this.iframeActorIds[0]);
			}
		});
	}

	addPhotoHover(actorId, $photo) {
		const photoPos = {
			y: $photo.offset().top,
			x: $photo.offset().left + $photo.width()
		};

		$photo.mouseenter(e => {

			database.get(actorId, actorData => {
				if (!actorData) {
					return;
				}

				if (actorId != this.lastDisplayedActorId) {
					this.displayPanel.resetUi();
					this.lastDisplayedActorId = actorId;
					this.displayPanel.loadValues(actorData);
				}

				this.displayPanel.show(photoPos);
			});

		}).mouseleave(() => this.displayPanel.hide());
	}

	watchStorage() {
		//this receives messages from the background messenger script
		chrome.runtime.onMessage.addListener(savedActor => {
			const $photo = $(`[href*=${savedActor.id}]`).parent();

			if (!$photo.hasClass(this.HAS_DATA_CSS_CLASS)) {
				$photo.addClass(this.HAS_DATA_CSS_CLASS);
			}

			if (savedActor.id == this.lastDisplayedActorId) {
				this.displayPanel.loadValues(savedActor);
				this.displayPanel.resetRefreshButton();
				this.displayPanel.resetUi();
			}

			const actorIframe = $('#iframe-' + savedActor.id);
			if (this.iframeActorIds.length) {

				actorIframe[0].remove();
				this.processedActorCount++;
				this.iframeActorIds.splice(0,1);

				const savedActors = this.processedActorCount + this.unprocessedActorCount - this.actorCount;
				this.progressBar.update(savedActors);
				const nextActor = this.iframeActorIds[0];
				nextActor && TitlePageUtil.makeActorIframe(nextActor);
			}
		});
	}
}