class TitlePageUtil {
	static makeActorIframe(actorId) {
		const iframe = $(`<iframe id=iframe-${actorId}></iframe>`);
		iframe.attr('src', `/name/${actorId}`);
		iframe.css('display', 'none');

		//runs the other half of this extension 
		$('body').prepend(iframe);
	}
}