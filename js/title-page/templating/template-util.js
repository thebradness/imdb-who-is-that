class TemplateUtil {
	static fixImgPaths(html) {
		const regex = /[-a-z]+\.svg/g;

		return html.replace(regex,
			filename => chrome.extension.getURL(`images/${filename}`)
		);
	}
}