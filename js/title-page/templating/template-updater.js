class TemplateUpdater {
	
	// from https://stackoverflow.com/a/39065147/2200951

	updateTemplate(templateHtml, target, values) {
		// console.log('imdw-values', target, values);
		const rendered = this.loadTemplate(templateHtml, values);
		$(target).html(rendered);
	}

	updateTemplateLoop(templateHtml, target, list) {
		const rendered = list.map(item => this.loadTemplate(templateHtml, item));
		$(target).html(rendered.join(''));
	}

	loadTemplate(templateHtml, values) {
		const template = templateHtml.split(/\{\{(.+?)\}\}/g);
		return template.map(this.render(values)).join('');
	}

	render(values) {
		//The split on the template goes: 
		//some other text, a variable, some other text, a variable
		//So when we loop through that list, we only want to replace 
		//the even things (with their config map value) 
		//and leave the other stuff alone.
		//using template.map(render(cfgobj)) calls that inner function on each item
		return function(tmpltKey, tkIdx) {
			return (tkIdx % 2) ? values[tmpltKey] : tmpltKey;
		}
	}
}