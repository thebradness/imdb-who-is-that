class TemplateFileLoader {

	loadHtml(filename) {
		return this.loadFile(filename)
			.then(r => r.text());
	}

	loadJson(filename) {
		return this.loadFile(filename)
			.then(r => r.json());
	}

	loadFile(filename) {
		const templateUrl = chrome.runtime.getURL(`templates/${filename}`);

		return fetch(templateUrl);
	}
}