class TemplateManager {

	actorPhotoTemplateHtml;
	creditTemplateHtml;
	knownForTemplateHtml;

	templater = new TemplateUpdater();
	fileLoader = new TemplateFileLoader();

	loadTemplates() {
		return this.fileLoader.loadHtml('credit.html')
			.then(creditHtml =>
				this.creditTemplateHtml = TemplateUtil.fixImgPaths(creditHtml)
			)
			.then(r =>
				this.fileLoader.loadHtml('known-for.html')
			)
			.then(knownForHtml =>
				this.knownForTemplateHtml = knownForHtml
			);
	}

	setMainTemplateHtml($mainWrapObj) {
		this.actorPhotoTemplateHtml = $mainWrapObj.find('#actor-photo').html();
		this.actorInfoTemplateHtml = $mainWrapObj.find('#actor-info-wrap').html();
	}

	updateTemplates(actorData) {
		this.templater
			.updateTemplate(this.actorPhotoTemplateHtml, '#actor-photo', actorData.actor_info);
		this.templater
			.updateTemplate(this.actorInfoTemplateHtml, '#actor-info-wrap', actorData.actor_info);

		this.templater
			.updateTemplateLoop(this.knownForTemplateHtml,
				'#known-for-wrap', actorData.known_fors);

		this.templater
			.updateTemplateLoop(this.creditTemplateHtml,
				'#credits-wrap', actorData.credits);

		this.templater
			.updateTemplate(this.creditTemplateHtml, '#current-page-info-wrap',
				this.getCurrentTitleCredit(actorData));
	}

	getCurrentTitleCredit(actorData) {
		let currentTitleId = window.location.pathname.split('/')[2];
		
		const seriesLink = $(Selectors.SERIES_LINK);
		if (seriesLink.length) {
			currentTitleId = seriesLink.attr('href').split('/')[2];
		}

		const currentTitleIdx = actorData.credits
			.findIndex(c => c.id == currentTitleId);

		return actorData.credits[currentTitleIdx];
	}
}