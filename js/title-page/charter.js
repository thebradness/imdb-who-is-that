class Charter {

	actorWorkGraph;

	setup() {
		this.buildChart();
		this.setOptions();
	}

	update(actorData) {
		const years = Object.keys(actorData.chart_values);

		this.actorWorkGraph.ages = null;
		const birthYear = actorData.actor_info.birth_year;
		if (birthYear) {
			this.actorWorkGraph.ages = years
				.reduce((map, year) => {
					map[year] = year - birthYear;
					return map;
				}, {});
		}

		this.actorWorkGraph.data.labels = years;
		this.actorWorkGraph.data.datasets[0].data = Object.values(actorData.chart_values);

		this.actorWorkGraph.update();
	}

	chartClick(event, graph) {
		//graph has to be passed in b/c this is run by the chartjs lib
		const point = graph.getElementsAtEventForMode(event, 'point', graph.options)[0];
		const year = graph.data.labels[point.index];

		$('body').find(Selectors.SEARCH_FIELD).val(year).change();
	}

	buildChart() {
		this.actorWorkGraph = new Chart('actor-workgraph', {
			type: 'line',
			data: {
				datasets: [{
					label: 'Credits',
					borderWidth: 1,
					borderColor: 'red',
					backgroundColor: 'red',
					trendlineLinear: {
						style: "rgba(131,131,131, .25)",
						lineStyle: "solid",
						width: 3,
					}
				}]
			},
			options: {
				tension: 0.3, //line smoothing
				pointRadius: 1.8,
				responsive: true, //fill container (set size by css)
				onClick: event => this.chartClick(event, this.actorWorkGraph),
				plugins: {
					tooltip: {
						callbacks: {
							label: function(context) {
								const credits = context.formattedValue;
								let display = ` ${credits} credit`;

								if (credits > 1) {
									display += 's';
								}

								if (context.chart.ages) {
									display += ` (age ${context.chart.ages[context.label]})`;
								}

								return display;
							}
						}
					}
				}
			}
		});
	}

	setOptions() {
		const opts = this.actorWorkGraph.options;

		opts.plugins.legend.display = false;

		opts.scales.x.grid.display = false;
		opts.scales.x.ticks.font = {
			size: 11
		};

		opts.scales.y.grid.display = false;
		opts.scales.y.ticks.font = {
			size: 11
		};
	}
}