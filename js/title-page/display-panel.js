class DisplayPanel {

	$imdWrap;

	position;
	currentActor;

	charter = new Charter();
	fileLoader = new TemplateFileLoader();
	templateManager = new TemplateManager();

	setup() {
		this.templateManager.loadTemplates()
		.then(r =>
			this.fileLoader.loadHtml('main.html')
			).then(mainHtml => {
				this.buildPanel(mainHtml);
			})
			.then(r => {
				this.charter.setup();
				this.addRefreshClick();
				this.addCreditFiltering();
				this.addFilterClearClick();
				this.addKnownForScrolling();
			});
		}

	show(position) {
		if (position) this.position = position; // b/c this is also called from below and position will be null

		const windowTop = window.scrollY;
		const windowHeight = window.innerHeight;
		const windowBottom = windowTop + windowHeight;
		
		let panelTop = this.position.y - 27; //so the top of the panel is just above the top of the actor headshot
		const bottomMargin = 50;
		const panelHeight = $('#imdw-wrap').height();
		const panelBottom = panelTop + panelHeight;

		//by default, the panel top aligns to the top of the actor photo
		if (panelBottom + bottomMargin > windowBottom) { //if it's cut off at the bottom

			const cutoffAmount = panelBottom - windowBottom;
			
			//leave all this crap here if I need to fix this again
			// console.log('----');
			// console.log('cutoff', cutoffAmount);
			// console.log('panelBottom', panelBottom, 'windowBottom', windowBottom);
			// console.log('panelHeight', panelHeight, 'windowHeight', windowHeight);

			if (windowHeight > panelHeight + bottomMargin) { //and there's enough room to pull it up
				panelTop -= cutoffAmount + bottomMargin; //bring it all the way up so the panel is sitting at the bottom of the window
			} else { //if there isn't enough room
				panelTop -= panelTop - windowTop; //bring it up as high as we can while still not getting cut off at the top
			}
		}

		if (panelTop < windowTop) { //if it's cut off at the top
			panelTop += windowTop - panelTop;
		}

		//have to start with display:none in CSS b/c otherwise 
		// - the page will move to show it in its loaded position
		//have to state display:flex here 
		// - b/c jQuery's .show() is just display:block
		this.$imdWrap.css({
			top: panelTop,
			left: this.position.x - 35, //scootch back a bit (especially needed on a laptop)
			display: 'flex'
		})
		.show();

		$(Selectors.SEARCH_FIELD).focus();
	}

	hide() {
		this.$imdWrap.hide();
		//I might want to do other stuff and having these functions as a pair feels like salt/pepper
	}

	buildPanel(mainHtml) {
		const htmlWithImgPaths = TemplateUtil.fixImgPaths(mainHtml);
		this.$imdWrap = $(htmlWithImgPaths);

		this.templateManager.setMainTemplateHtml(this.$imdWrap); //need to save the original html with the curly braces before they get replaced

		//panel also needs to keep open and then close itself based on mouse
		this.$imdWrap.hover(
			() => this.show(),
			() => this.hide() //comment for testing
		);

		$('body').prepend(this.$imdWrap);
	}

	loadValues(actorData) {
		this.currentActor = actorData;
		this.charter.update(actorData);
		this.templateManager.updateTemplates(actorData);
	}

	resetUi() {
		$(Selectors.CLEAR_BUTTON).hide();
		$(Selectors.SEARCH_FIELD).val('');
		window.setTimeout(() => {
			document.querySelector('#credits-wrap').scrollTo(0, 0);
			document.querySelector('#known-for-wrap').scrollTo(0, 0);
		});
	}

	addCreditFiltering() {
		const $searchField = $(Selectors.SEARCH_FIELD);
		const $clearBtn = $(Selectors.CLEAR_BUTTON);

		const onFilter = e => {
			if (e.key == 'Escape') {
				$(Selectors.CLEAR_BUTTON).click();
				return;
			}

			document.querySelector('#credits-wrap').scrollTo(0, 0);

			const searchText = $searchField.val().toLowerCase().trim();

			if (searchText)
				$clearBtn.show();
			else
				$clearBtn.hide();

			$('body').find('#credits-wrap .item-group')
			.each(function() {
				const $credit = $(this);

				if ($credit.text().toLowerCase().includes(searchText))
					$credit.show();
				else
					$credit.hide();
			});

			if (e.type == 'change') {
				//if we're sending a year in from the graph, focus the box
				// - so you can hit escape to clear it
				$(Selectors.SEARCH_FIELD).focus();
			}
		};

		$searchField.keyup(onFilter); //change only fires when you leave the field
		$searchField.change(onFilter); //fired when you click a chart point
	}

	addKnownForScrolling() {
		const isMac = navigator.userAgent.indexOf('Macintosh');
		if (isMac > -1) return; //mac trackpad already handles this

		//this function gets run from somewhere else 
		// - so we have to ship it directly 
		// - (can't have it as a function in this class)
		const scrollHorizontally = e => {
			//from https://stackoverflow.com/a/15343916/2200951
			var direction = e.wheelDelta > 0 ? 1 : -1;
			document.querySelector('#known-for-wrap')
			.scrollLeft -= direction * 40;
			e.preventDefault(); //don't also scroll vertically
		};

		document.querySelector('#known-for-wrap')
		.addEventListener("mousewheel", scrollHorizontally, false);
	}

	addFilterClearClick() {
		$(Selectors.CLEAR_BUTTON).click(function() {
			$(Selectors.SEARCH_FIELD).val('').change().focus();
		});
	}

	addRefreshClick() {
		const self = this;

		$(Selectors.REFRESH_BUTTON).click(function() {
			$(Selectors.REFRESH_ICON).hide();
			$(Selectors.REFRESH_LABEL).text('Refreshing...');

			TitlePageUtil.makeActorIframe(self.currentActor.actor_info.id);
		});
	}

	resetRefreshButton() {
		$(Selectors.REFRESH_ICON).show();
		$(Selectors.REFRESH_LABEL).text('Refresh');
	}
}