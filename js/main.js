

const database = new Database();
database.setup(main); //don't do anything until after the db is ready


function main() {
	if (isActorPage()) {
		new ActorPageManager().run();
	} else {
		new TitlePageManager().run();
	}
}

function isActorPage() {
	const urlParts = window.location.pathname.split('/');
	return urlParts[1] == 'name';
}