template = 'hello|goodbye|hi|bye'.split('|');
console.log(template.map(render([1,2])));

function render(vals) {
	return function(orig, idx) {
		return (idx % 2 ? 'no' : orig);
	}
}