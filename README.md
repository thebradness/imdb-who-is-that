# Changelog (sortof)
* **2023-01-04** *Total rewrite and UI redesign*

# What is this?
* This is a fun little Chrome extension I made to quickly see anything else an actor has been in (as well as their age then and now) without having to actually go to each actor's page. 
* This way, you can open up an IMDb page for a movie you're watching and when you say, "Wait... who is that?" all you have to do is put your mouse over that actor's photo to see what else they were in.

# How do I install it?
1. Download the source (the **Downloads** link of Bitbucket's left sidebar and then click **Download repository**) and unzip it to any directory.
    * Or... If you're familiar with Git, just clone it. IMDb changes their site occasionally and this extension breaks. I'll fix it when they do, and it'll be much easier for you to just pull the changes down.
1. Go to the Chrome menu > More Tools > Extensions.
1. Switch on **Developer mode** (upper-right corner).
1. Click the **Load unpacked** button (upper-left corner).
1. Choose the directory the source zip file was unzipped to in step 1.
1. Go to any TV or movie page at [IMDb.com like this one](https://www.imdb.com/title/tt0098258/) and mouse-hover on any actor photo in the cast list.
1. Enjoy!

# How does it work?
* When you go to a cast list page, it opens an iframe for each individual actor, scrapes their data and stores it in [IndexedDB](https://javascript.info/indexeddb) (a little database in your browser on your computer). 
* After an actor is saved, it won't look that actor up again unless you click the refresh button in the panel this extension creates or you go to that actor's page.

# Want celebrity net worth too?!
1. Get an API key from [API Ninjas](https://api-ninjas.com/) (it's free).
1. Put it in the **net-worth-api-key.js** file.
1. That's it!
